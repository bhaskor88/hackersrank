/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackersrank.newyearchaos;

/**
 *
 * @author admin
 */
public class NewYearChaos {

    public static void minimumBribes(int[] q) {
        int lastDiff = 0;
        boolean caotic = false;
        for (int i = 1; i <= q.length; i++) {
            if (q[i - 1] > i) {
                int diff = q[i - 1] - i;
                if (diff > 2) {
                    caotic = true;
                    break;
                }
//                if (diff > lastDiff) {
                lastDiff += diff;
//                }
            }
        }
        if (caotic) {
            System.out.println("Too chaotic");
        } else {
            System.out.println(lastDiff);
        }
    }
}
