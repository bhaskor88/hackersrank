/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackersrank.leftrotation;

/**
 *
 * @author admin
 */
public class LeftRotation {

//    public static void main(String[] args) {
//        printArray(rotLeft(new int[]{1, 2, 3, 4, 5}, 0)); 
//        //1 2 3 4 5
//        printArray(rotLeft(new int[]{1, 2, 3, 4, 5}, 4)); 
//        //5 1 2 3 4
//        printArray(rotLeft(new int[]{1, 2, 3, 4, 5}, 8)); 
//        //4 5 1 2 3
//    }

    // Complete the rotLeft function below.
    public static int[] rotLeft(int[] a, int d) {
        int[] res = new int[a.length];
        if (d > a.length) {
            d = d - a.length;
        }
        for (int i = 0; i < a.length; i++) {
            if (i + d >= a.length) {
                res[i] = a[i + d - a.length];
            } else {
                res[i] = a[i + d];
            }
        }
        return res;
    }

//    public static void printArray(int arr[]) {
//        for (int i = 0; i < arr.length; i++) {
//            System.out.print(arr[i] + " ");
//        }
//    }
}
