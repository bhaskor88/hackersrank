/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackersrank;

import hackersrank.hourglass.HourGlass;
import hackersrank.leftrotation.LeftRotation;
import hackersrank.newyearchaos.NewYearChaos;

/**
 *
 * @author admin
 */
public class HackersRank {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        NewYearChaos.minimumBribes(new int[]{2, 1, 5, 3, 4});
//        NewYearChaos.minimumBribes(new int[]{2, 5, 1, 3, 4});
//        NewYearChaos.minimumBribes(new int[]{1, 2, 5, 3, 4, 7, 8, 6});
        NewYearChaos.minimumBribes(new int[]{5, 1, 2, 3, 7, 8, 6, 4});
        NewYearChaos.minimumBribes(new int[]{1, 2, 5, 3, 7, 8, 6, 4});
    }

    public static void printArray(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
