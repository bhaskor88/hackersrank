/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hackersrank.hourglass;

/**
 *
 * @author admin
 */
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class HourGlass {

    public static void main(String[] args) {
        System.out.println(HourGlass.hourglassSum(new int[][]{
            {1, 1, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0},
            {1, 1, 1, 0, 0, 0},
            {0, 0, 2, 4, 4, 0},
            {0, 0, 0, 2, 0, 0},
            {0, 0, 1, 2, 4, 0}
        }));
        //19
        System.out.println(hourglassSum(new int[][]{
            {-1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1},
            {-1, -1, -9, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1}
        }));
        //-7
    }

    public static int hourglassSum(int[][] arr) {
        int sum = -Integer.MAX_VALUE;

        for (int i = 0; i < arr.length - 2; i++) {
            for (int j = 0; j < arr[0].length - 2; j++) {
                int res = calculateHourGlassSum(i, j, arr);
                if (res > sum) {
                    sum = res;
                }
            }
        }
        return sum;
    }

    public static int calculateHourGlassSum(int i, int j, int[][] arr) {
        int res = 0;
        for (int k = i; k < i + 3 && k < arr.length; k++) {
            for (int l = j; l < j + 3 && l < arr[0].length; l++) {
                if (k == i + 1 && (l == j || l == j + 2)) {
                    continue;
                } else {
                    res += arr[k][l];
                }
            }
        }
        return res;
    }
}
